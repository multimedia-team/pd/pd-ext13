lib.name = ext13
class.sources = \
	catch13~.c \
	filesize.c \
	ftos.c \
	kalashnikov.c \
	mandelbrot.c \
	mandelbrot~.c \
	messages.c \
	openpatch.c \
	piperead~.c \
	pipewrite~.c \
	receive13.c \
	receive13~.c \
	scramble~.c \
	send13.c \
	send13~.c \
	sfwrite13~.c \
	streamin13~.c \
	streamout13~.c \
	strippath.c \
	throw13~.c \
	wavinfo.c \
	$(empty)
lib.setup.sources = ext13.c

define forLinux
class.sources += \
	cdplayer.c \
	ossmixer.c \
	promiscous~.c \
	$(empty)
endef

datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(wildcard *.md) \
	CHANGES \
	$(empty)

datadirs = \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
